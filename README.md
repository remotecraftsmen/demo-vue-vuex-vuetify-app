# Todo Application with Vue, Vuex and Vuetify

##Link for Live Demo : 
    https://vue-vuex-vuetify-demo.rmtcfm.com/

### Project setup
```
npm install
```
###Creatin .env file
    File has to start with .env. Possible names : 
        .env
        .env.local
        .env.development
        .env.development.local
        .env.production
        .env.production.local
        .env.staging  # this is for a custom --mode staging
        .env.staging.local
    Only variables that start with VUE_APP_ will be recognized
More info : [Environment Variables and Modes](https://cli.vuejs.org/guide/mode-and-env.html#modes).

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
