import Login from './components/Login.vue';
import Register from './components/Register.vue';
import PageNotFound from './components/PageNotFound.vue';
import TodoList from './components/TodoList.vue';
import VueRouter from 'vue-router';

const routes = [
    {path: '/', component: Login},
    {path: '/login', component: Login},
    {path: '/registration', component: Register},
    {path: '/todo', meta: {requiresAuth: true}, component: TodoList},
    {path: "*", component: PageNotFound}
];

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {

    if (to.matched.some(m => m.meta.requiresAuth)) {
        if (sessionStorage.getItem('token')) {
            return next();
        } else {
            return next({path: '/login'})
        }
    }

    return next()
});

export default router;


