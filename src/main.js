import App from './App.vue';
import Axios from 'axios';
import router from './routes'
import {store} from './store';
import Vue from 'vue';
import Vuetify from 'vuetify';
import VueRouter from 'vue-router';
import 'bootstrap/dist/js/bootstrap.min';
import './plugins/vuetify'
import './styles/global.scss';

if (sessionStorage.getItem('token')) {
    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + sessionStorage.getItem('token');
}
Axios.defaults.baseURL = process.env.VUE_APP_API_URL;
Vue.prototype.$http = Axios;

Vue.use(Vuetify);
Vue.use(VueRouter);

Vue.config.devtools = false;
Vue.config.performance = false;

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App)
});
