import Vue from 'vue';
import Vuex from 'vuex'
import Axios from 'axios'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        todos: [],
        token: '',
    },
    getters: {
        token: state => {
            return state.token;
        },
        todos: state => {
            return state.todos;
        }
    },
    mutations: {
        updateToken(state, newToken) {
            sessionStorage.setItem('token', newToken);
            if (sessionStorage.getItem('token')) {
                state.token = sessionStorage.getItem('token');
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + state.token;
            }
        },
        removeToken(state) {
            sessionStorage.removeItem('token');
            state.token = null;
        },
        addTodo: (state, payload) => {
            state.todos.push(payload)
        },
        removeTodo: (state, payload) => {
            let indexToDelete = state.todos.indexOf(payload);
            state.todos.splice(indexToDelete, 1);
        },
        updateTodo: (state, payload) => {
            payload.name = payload.name;
        },
        changeCompletedState: (state, payload) => {
            payload.completed = !payload.completed
        },
        getTodos: (state, payload) => {
            state.todos = payload;
        },
        sortTodos: (state) => {
            state.todos.sort(function (x, y) {
                return x.completed - y.completed;
            });
        }
    },
    actions: {
        sortTodos({commit}) {
            commit('sortTodos')

        },
        updateToken({commit}, payload) {
            commit('updateToken', payload);
        },
        removeToken(state) {
            sessionStorage.removeItem('token');
            state.token = null;
        },
        addTodo({commit}, payload) {
            Axios.post('/todos', payload).then((response) => {
                commit('addTodo', response.data)
            })
        },
        removeTodo({commit}, payload) {
            Axios.delete('/todos/' + payload.id).then((response) => {
                commit('removeTodo', payload);
            })
        },
        updateTodo({commit}, payload) {
            Axios.patch('/todos/' + payload.id, {name: payload.name}).then((response) => {
                commit('updateTodo', payload)
            })
        },
        changeCompletedState({commit}, payload) {
            Axios.patch('/todos/' + payload.id, {completed: !payload.completed}).then((response) => {
                commit('changeCompletedState', payload)
            })
        },
        async getTodos({commit}) {
            await Axios.get('/todos').then((response) => {
                commit('getTodos', response.data.todos)
            })
        }
    }
});
